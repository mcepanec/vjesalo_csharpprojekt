﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Vjesalo
{
    public class DeletionManager
    {
        public void DeleteWord(string wordValue, string categoryValue)
        {
            using (var dbContext = new EntityDB())
            {
                var category = dbContext.Categories.FirstOrDefault(c => c.CategoryValue == categoryValue);

                var wordEntity = dbContext.Words
                    .FirstOrDefault(w => w.WordValue == wordValue && w.CategoryID == category.Id);

                if (wordEntity != null)
                {
                    dbContext.Words.Remove(wordEntity);
                    dbContext.SaveChanges();
                }
            }
        }
    }
}
