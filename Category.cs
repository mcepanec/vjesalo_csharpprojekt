﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vjesalo
{
    public partial class Category
    {
        public int Id { get; set; }

        [Column("Category")]
        [StringLength(50)]
        public string CategoryValue { get; set; }
    }
}
