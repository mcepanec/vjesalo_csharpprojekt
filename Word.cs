namespace Vjesalo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Word
    {
        public int Id { get; set; }

        [Column("Word")]
        [StringLength(50)]
        public string WordValue { get; set; }
        public int CategoryID { get; set; }
    }
}
