using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Remoting.Contexts;

namespace Vjesalo
{
    public partial class EntityDB : DbContext
    {
        public EntityDB()
            : base("name=EntityDB")
        {
        }

        public virtual DbSet<Word> Words { get; set; }
        public DbSet<Category> Categories { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Word>()
                .Property(e => e.WordValue)
                .IsUnicode(true)
                .HasColumnType("nvarchar")
                .HasColumnAnnotation("Collate", "Croatian_CI_AS");

            modelBuilder.Entity<Category>()
                .Property(e => e.CategoryValue)
                .IsUnicode(true)
                .HasColumnType("nvarchar")
                .HasColumnAnnotation("Collate", "Croatian_CI_AS");
            base.OnModelCreating(modelBuilder);
        }
    }
}
