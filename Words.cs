﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vjesalo
{
    public class Words
    {
        private EntityDB db;
        public Words()
        {
            db = new EntityDB();
        }

        public IEnumerable<Word> All()
        {
            return
                from word in db.Words
                orderby word.WordValue
                select word;
        }

        public IEnumerable<string> GetAllWordsWithCategories()
        {
            var query =
                from word in db.Words
                join category in db.Categories on word.CategoryID equals category.Id
                orderby word.WordValue
                select word.WordValue + " - " + category.CategoryValue; 

            return query.ToList();
        }


        public IEnumerable<Word> Random()
        {
            return db.Words
                     .OrderBy(w => Guid.NewGuid()) 
                     .Take(1); 
        }

        public IEnumerable<string> GetFilteredWordsWithCategories(string searchText)
        {
            var query =
                from word in db.Words
                join category in db.Categories on word.CategoryID equals category.Id
                where word.WordValue.ToLower().Contains(searchText.ToLower()) || category.CategoryValue.ToLower().Contains(searchText.ToLower())
                orderby word.WordValue
                select word.WordValue + " - " + category.CategoryValue;

            return query.ToList();
        }
    }
}
