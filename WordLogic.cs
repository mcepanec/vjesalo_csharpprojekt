﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Vjesalo
{
    public class WordLogic
    {
        private string secretWord;
        private HashSet<char> guessedLetters;

        public string MaskedWord { get; private set; }

        public WordLogic(string word)
        {
            secretWord = word.ToUpper();
            guessedLetters = new HashSet<char>();
            MaskedWord = GetMaskedWord();
        }

        public bool GuessLetter(char letter)
        {
            letter = char.ToUpper(letter);
            guessedLetters.Add(letter);
            MaskedWord = GetMaskedWord();
            return secretWord.Contains(letter);
        }

        public bool AllLettersGuessed()
        {
            return !secretWord.Any(c => !guessedLetters.Contains(c));
        }

        private string GetMaskedWord()
        {
            string maskedWord = "";
            foreach (char letter in secretWord)
            {
                if (guessedLetters.Contains(letter))
                {
                    maskedWord += letter;
                }
                else
                {
                    maskedWord += "_ ";
                }
            }
            return maskedWord;
        }

        public string GetGuessedLetters()
        {
            return string.Join(", ", guessedLetters);
        }
    }
}
