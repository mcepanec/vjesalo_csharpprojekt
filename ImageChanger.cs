﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Vjesalo
{
    
    public class ImageChanger
    {
        private List<Image> images;
        private int CurrentImageIndex;
        private int ImageCount;

        public bool EndGameCheck()
        {
            if (CurrentImageIndex == images.Count - 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private void LoadImages()
        {
            images.Add(Vjesalo.Properties.Resources.omca);
            images.Add(Vjesalo.Properties.Resources.omca_glava);
            images.Add(Vjesalo.Properties.Resources.omca_glava_trup);
            images.Add(Vjesalo.Properties.Resources.omca_glava_trup_ruka);
            images.Add(Vjesalo.Properties.Resources.omca_glava_trup_ruka_ruka);
            images.Add(Vjesalo.Properties.Resources.omca_glava_trup_ruka_ruka_noga);
            images.Add(Vjesalo.Properties.Resources.mrtav_skroz_verzija_2);
        }

        public ImageChanger()
        {
            CurrentImageIndex = -1;
            images = new List<Image>();
            ImageCount = images.Count;
            LoadImages();
        }

        public Image GetNextImage()
        {
            if (images.Count == 0)
            {
                throw new InvalidOperationException("Nema slika");
            }

                CurrentImageIndex += 1;
                return images[CurrentImageIndex];

        }

        public Image GetCurrentImage()
        {
            if (images.Count == 0)
            {
                throw new InvalidOperationException("Nema Pohranjenih Slika");
            }

            return images[CurrentImageIndex];
        }

        public void Reset()
        {
            CurrentImageIndex = -1;
        }
    }
}
