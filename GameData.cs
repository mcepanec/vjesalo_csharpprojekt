﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace Vjesalo
{
    [Serializable]
    public class GameData
    {
        public int CorrectGuesses { get; set; }
        public int WrongGuesses { get; set; }
        public string WordToGuess { get; set; }
        public bool IsWin { get; set; }

        public GameData(int CorrectGuesses, int WrongGuesses, string WordToGuess, bool IsWin) {
            this.CorrectGuesses = CorrectGuesses;
            this.WrongGuesses = WrongGuesses;
            this.WordToGuess = WordToGuess;
            this.IsWin = IsWin;
        }
    }
}
