﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Vjesalo
{
    public partial class ResetConfirmation : Form
    {
        public ResetConfirmation()
        {
            InitializeComponent();
        }

        private void btnNePonavljanje_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnDaPonavljanje_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Yes;
        }

        private void btnConfToMenu_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        public void SetMessage(string message)
        {
            lblUnlucky.Text = message;
        }
    }
}
