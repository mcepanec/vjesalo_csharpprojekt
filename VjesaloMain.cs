﻿using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Vjesalo
{
    public partial class VjesaloMain : Form
    {
        private ImageChanger imageChanger = new ImageChanger();
        private ResetConfirmation confirmation = new ResetConfirmation();
        private Words words = new Words();
        private WordLogic wordLogic;
        private DatabaseInserter saveManager;

        private int guesses;
        private int wrongGuesses;
        private bool isWin;
        private string wordToGuess;

        public VjesaloMain()
        {
            InitializeComponent();
            InitializeGame();
        }

        private void InitializeGame()
        {
            var randomWord = words.Random().FirstOrDefault();
            wordToGuess = randomWord?.WordValue ?? string.Empty;
            wordLogic = new WordLogic(wordToGuess);
            saveManager = new DatabaseInserter();
            UpdateMaskedWordLabel();
        }

        private void btnGuess_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtLetterToGuess.Text))
            {
                MessageBox.Show("Molim vas unesite slovo!");
                return;
            }

            ProcessGuess(txtLetterToGuess.Text.ToUpper()[0]);
            txtLetterToGuess.Clear();
        }

        private void ProcessGuess(char guess)
        {
            if (wordLogic.GuessLetter(guess))
            {
                lblRightWrong.Text = "Točno";
                guesses++;
            }
            else
            {
                HandleWrongGuess();
            }

            UpdateGameStatus();
        }

        private void HandleWrongGuess()
        {
            lblRightWrong.Text = "Netočno";
            picboxHangman.Image = imageChanger.GetNextImage();
            guesses++;
            wrongGuesses++;

            if (imageChanger.EndGameCheck())
            {
                isWin = false;
                EndGame();
            }
        }

        private void UpdateGameStatus()
        {
            UpdateMaskedWordLabel();
            UpdateGuessedLettersLabel();

            if (wordLogic.AllLettersGuessed())
            {
                isWin = true;
                EndGame();
            }
        }

        private void EndGame()
        {
            SaveGameData();

            confirmation.SetMessage(isWin ? "Pobjedili ste!" : "Objesili ste ga!");
            var result = confirmation.ShowDialog();
            HandleDialog(result);
        }

        private void SaveGameData()
        {
            string filePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "temp", "game_data.json");
            var gameData = new GameData(guesses, wrongGuesses, wordToGuess, isWin);
            var serializer = new GameSerialization();
            serializer.SaveGame(gameData, filePath);
        }

        private void HandleDialog(DialogResult result)
        {
            if (result == DialogResult.Yes)
            {
                ResetGame();
            }
            else if (result == DialogResult.Cancel)
            {
                ReturnToMainMenu();
            }
        }

        private void ResetGame()
        {
            InitializeGame();
            UpdateMaskedWordLabel();
            UpdateGuessedLettersLabel();
            imageChanger.Reset();
            picboxHangman.Image = null;
        }

        private void ReturnToMainMenu()
        {
            this.Hide();
            var mainMenu = new MainMenu();
            mainMenu.ShowDialog();
            this.Close();
        }

        private void UpdateMaskedWordLabel()
        {
            lblLetterDisplay.Text = wordLogic.MaskedWord;
        }

        private void UpdateGuessedLettersLabel()
        {
            lblGuessedLetters.Text = "Pogođena slova: " + wordLogic.GetGuessedLetters();
        }
    }
}
