﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.IO;

namespace Vjesalo
{
    public partial class MainMenu : Form
    {
        private Words words = new Words();
        DatabaseInserter loadManager;
        DeletionManager deletionManager;
        public MainMenu()
        {
            InitializeComponent();
            showAllWords();
            loadManager = new DatabaseInserter();
            deletionManager = new DeletionManager();
        }

        private void btnPlay_Click(object sender, EventArgs e)
        {
            this.Hide();
            VjesaloMain vjesaloMain = new VjesaloMain();
            vjesaloMain.ShowDialog();
            this.Close();
        }

        private void showAllWords()
        {
            lboxSveRijeci.Items.Clear();
            foreach (var word in words.GetAllWordsWithCategories())
            {
                lboxSveRijeci.Items.Add(word);
            }
        }

        private async void btnDodajRijeci_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.Filter = "JSON files (*.json)|*.json|All files (*.*)|*.*";
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    string filePath = openFileDialog.FileName;
                    await loadManager.ProcessJsonFileAsync(filePath);
                    showAllWords(); 
                }
            }
        }



        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (lboxSveRijeci.SelectedItem == null)
            {
                MessageBox.Show("Molim vas odaberite riječ za obrisat", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var selectedItem = lboxSveRijeci.SelectedItem.ToString();
            var parts = selectedItem.Split(new[] { " - " }, StringSplitOptions.None);
            var wordValue = parts[0];
            var categoryValue = parts[1];

            deletionManager.DeleteWord(wordValue, categoryValue);
            showAllWords();
        }

        private void FilterWords()
        {
            string searchText = txtSearch.Text.ToLower();
            lboxSveRijeci.Items.Clear();

            if (string.IsNullOrWhiteSpace(searchText))
            {
                foreach (var word in words.GetAllWordsWithCategories())
                {
                    lboxSveRijeci.Items.Add(word);
                }
            }
            else
            {
                foreach (var word in words.GetFilteredWordsWithCategories(searchText))
                {
                    lboxSveRijeci.Items.Add(word);
                }
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            FilterWords();
        }
    }
}

