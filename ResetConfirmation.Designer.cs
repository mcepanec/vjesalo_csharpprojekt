﻿namespace Vjesalo
{
    partial class ResetConfirmation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblBiLiHtjeli = new System.Windows.Forms.Label();
            this.lblUnlucky = new System.Windows.Forms.Label();
            this.btnDaPonavljanje = new System.Windows.Forms.Button();
            this.btnNePonavljanje = new System.Windows.Forms.Button();
            this.btnConfToMenu = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblBiLiHtjeli
            // 
            this.lblBiLiHtjeli.AutoSize = true;
            this.lblBiLiHtjeli.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBiLiHtjeli.Location = new System.Drawing.Point(120, 33);
            this.lblBiLiHtjeli.Name = "lblBiLiHtjeli";
            this.lblBiLiHtjeli.Size = new System.Drawing.Size(104, 16);
            this.lblBiLiHtjeli.TabIndex = 0;
            this.lblBiLiHtjeli.Text = "želite li ponoviti?";
            // 
            // lblUnlucky
            // 
            this.lblUnlucky.AutoSize = true;
            this.lblUnlucky.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnlucky.Location = new System.Drawing.Point(87, 9);
            this.lblUnlucky.Name = "lblUnlucky";
            this.lblUnlucky.Size = new System.Drawing.Size(0, 24);
            this.lblUnlucky.TabIndex = 1;
            // 
            // btnDaPonavljanje
            // 
            this.btnDaPonavljanje.Location = new System.Drawing.Point(33, 108);
            this.btnDaPonavljanje.Name = "btnDaPonavljanje";
            this.btnDaPonavljanje.Size = new System.Drawing.Size(67, 23);
            this.btnDaPonavljanje.TabIndex = 2;
            this.btnDaPonavljanje.Text = "Da";
            this.btnDaPonavljanje.UseVisualStyleBackColor = true;
            this.btnDaPonavljanje.Click += new System.EventHandler(this.btnDaPonavljanje_Click);
            // 
            // btnNePonavljanje
            // 
            this.btnNePonavljanje.Location = new System.Drawing.Point(142, 108);
            this.btnNePonavljanje.Name = "btnNePonavljanje";
            this.btnNePonavljanje.Size = new System.Drawing.Size(67, 23);
            this.btnNePonavljanje.TabIndex = 3;
            this.btnNePonavljanje.Text = "Ne ";
            this.btnNePonavljanje.UseVisualStyleBackColor = true;
            this.btnNePonavljanje.Click += new System.EventHandler(this.btnNePonavljanje_Click);
            // 
            // btnConfToMenu
            // 
            this.btnConfToMenu.Location = new System.Drawing.Point(249, 108);
            this.btnConfToMenu.Name = "btnConfToMenu";
            this.btnConfToMenu.Size = new System.Drawing.Size(67, 23);
            this.btnConfToMenu.TabIndex = 4;
            this.btnConfToMenu.Text = "Menu";
            this.btnConfToMenu.UseVisualStyleBackColor = true;
            this.btnConfToMenu.Click += new System.EventHandler(this.btnConfToMenu_Click);
            // 
            // ResetConfirmation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(352, 143);
            this.Controls.Add(this.btnConfToMenu);
            this.Controls.Add(this.btnNePonavljanje);
            this.Controls.Add(this.btnDaPonavljanje);
            this.Controls.Add(this.lblUnlucky);
            this.Controls.Add(this.lblBiLiHtjeli);
            this.Name = "ResetConfirmation";
            this.Text = "ResetConfirmation";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblBiLiHtjeli;
        private System.Windows.Forms.Label lblUnlucky;
        private System.Windows.Forms.Button btnDaPonavljanje;
        private System.Windows.Forms.Button btnNePonavljanje;
        private System.Windows.Forms.Button btnConfToMenu;
    }
}