﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Vjesalo;

public class DatabaseInserter
{
    public async Task ProcessJsonFileAsync(string filePath)
    {
        try
        {
            string jsonText;
            using (StreamReader fileReader = new StreamReader(filePath))
            {
                jsonText = await fileReader.ReadToEndAsync();
            }

            List<WordCategoryDto> wordCategoryList = JsonConvert.DeserializeObject<List<WordCategoryDto>>(jsonText);

            await Task.Run(() => SaveToDatabase(wordCategoryList));
        }
        catch (Exception ex)
        {
            MessageBox.Show($"Problem s procesiranjem: {ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }

    private void SaveToDatabase(List<WordCategoryDto> wordCategoryList)
    {
        int insertedCount = 0;
        HashSet<string> addedWords = new HashSet<string>();

        var uniqueCategories = wordCategoryList
            .Select(wc => char.ToUpper(wc.Category[0]) + wc.Category.Substring(1))
            .Distinct()
            .ToList();

        using (var dbContext = new EntityDB())
        {
            foreach (var categoryValue in uniqueCategories)
            {
                if (!dbContext.Categories.Any(c => c.CategoryValue == categoryValue))
                {
                    var category = new Category { CategoryValue = categoryValue };
                    dbContext.Categories.Add(category);
                }
            }
            dbContext.SaveChanges();
        }

        using (var dbContext = new EntityDB())
        {
            foreach (var wordCategory in wordCategoryList)
            {
                var wordValue = wordCategory.Word.ToUpper();
                var categoryValue = char.ToUpper(wordCategory.Category[0]) + wordCategory.Category.Substring(1);

                var category = dbContext.Categories.FirstOrDefault(c => c.CategoryValue == categoryValue);
                if (category == null)
                {
                    category = new Category { CategoryValue = categoryValue };
                    dbContext.Categories.Add(category);
                    dbContext.SaveChanges();
                }

                if (!dbContext.Words.Any(w => w.WordValue.ToUpper() == wordValue && w.CategoryID == category.Id) && !addedWords.Contains(wordValue))
                {
                    var word = new Word { WordValue = wordValue, CategoryID = category.Id };
                    dbContext.Words.Add(word);
                    insertedCount++;
                    addedWords.Add(wordValue);
                }
            }
            dbContext.SaveChanges();
        }

        MessageBox.Show($"{insertedCount} riječi dodano u bazu.", "Uspješan unos", MessageBoxButtons.OK, MessageBoxIcon.Information);
    }
}
