﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;

namespace Vjesalo
{

    public class GameSerialization
    {
        
        public void SaveGame(GameData gameData, string filePath)
        {

            string jsonData = JsonConvert.SerializeObject(gameData, Formatting.Indented);
            File.WriteAllText(filePath, jsonData);
        }
    }

}
