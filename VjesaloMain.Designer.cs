﻿namespace Vjesalo
{
    partial class VjesaloMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtLetterToGuess = new System.Windows.Forms.TextBox();
            this.btnGuess = new System.Windows.Forms.Button();
            this.lblLetterDisplay = new System.Windows.Forms.Label();
            this.picboxHangman = new System.Windows.Forms.PictureBox();
            this.lblGuessedLetters = new System.Windows.Forms.Label();
            this.lblRightWrong = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picboxHangman)).BeginInit();
            this.SuspendLayout();
            // 
            // txtLetterToGuess
            // 
            this.txtLetterToGuess.Location = new System.Drawing.Point(320, 315);
            this.txtLetterToGuess.Name = "txtLetterToGuess";
            this.txtLetterToGuess.Size = new System.Drawing.Size(125, 20);
            this.txtLetterToGuess.TabIndex = 0;
            // 
            // btnGuess
            // 
            this.btnGuess.Location = new System.Drawing.Point(348, 341);
            this.btnGuess.Name = "btnGuess";
            this.btnGuess.Size = new System.Drawing.Size(75, 23);
            this.btnGuess.TabIndex = 1;
            this.btnGuess.Text = "Pogodi";
            this.btnGuess.UseVisualStyleBackColor = true;
            this.btnGuess.Click += new System.EventHandler(this.btnGuess_Click);
            // 
            // lblLetterDisplay
            // 
            this.lblLetterDisplay.AutoSize = true;
            this.lblLetterDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.lblLetterDisplay.Location = new System.Drawing.Point(326, 250);
            this.lblLetterDisplay.Name = "lblLetterDisplay";
            this.lblLetterDisplay.Size = new System.Drawing.Size(0, 25);
            this.lblLetterDisplay.TabIndex = 2;
            // 
            // picboxHangman
            // 
            this.picboxHangman.ImageLocation = "";
            this.picboxHangman.Location = new System.Drawing.Point(297, -33);
            this.picboxHangman.Name = "picboxHangman";
            this.picboxHangman.Size = new System.Drawing.Size(175, 280);
            this.picboxHangman.TabIndex = 3;
            this.picboxHangman.TabStop = false;
            // 
            // lblGuessedLetters
            // 
            this.lblGuessedLetters.AutoSize = true;
            this.lblGuessedLetters.Location = new System.Drawing.Point(570, 321);
            this.lblGuessedLetters.Name = "lblGuessedLetters";
            this.lblGuessedLetters.Size = new System.Drawing.Size(0, 13);
            this.lblGuessedLetters.TabIndex = 4;
            // 
            // lblRightWrong
            // 
            this.lblRightWrong.AutoSize = true;
            this.lblRightWrong.Location = new System.Drawing.Point(349, 299);
            this.lblRightWrong.Name = "lblRightWrong";
            this.lblRightWrong.Size = new System.Drawing.Size(0, 13);
            this.lblRightWrong.TabIndex = 5;
            // 
            // VjesaloMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(774, 441);
            this.Controls.Add(this.lblRightWrong);
            this.Controls.Add(this.lblGuessedLetters);
            this.Controls.Add(this.picboxHangman);
            this.Controls.Add(this.lblLetterDisplay);
            this.Controls.Add(this.btnGuess);
            this.Controls.Add(this.txtLetterToGuess);
            this.Name = "VjesaloMain";
            this.Text = "Vješalo";
            ((System.ComponentModel.ISupportInitialize)(this.picboxHangman)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtLetterToGuess;
        private System.Windows.Forms.Button btnGuess;
        private System.Windows.Forms.Label lblLetterDisplay;
        private System.Windows.Forms.PictureBox picboxHangman;
        private System.Windows.Forms.Label lblGuessedLetters;
        private System.Windows.Forms.Label lblRightWrong;
    }
}

